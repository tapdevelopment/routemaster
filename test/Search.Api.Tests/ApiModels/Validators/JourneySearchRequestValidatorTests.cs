﻿

using System;
using System.Linq;
using Search.Api.ApiModels;
using Search.Api.ApiModels.Validators;
using Xunit;

namespace SearchService.Test.ApiModels.Validators
{
    public class JourneySearchRequestValidatorTests
    {
        [Fact] 
        public void JourneySearchRequestValidator_Fails_WhenDepartureTimeIsInThePast()
        {
            var validator = new JourneySearchRequestValidator();

            var request = new JourneySearchRequest()
            {
                Destination = new JourneySearchRequestLocation
                {
                    Code = "123"
                },
                Origin = new JourneySearchRequestLocation
                {
                    Code = "123"
                },
                DepartureTime = DateTimeOffset.Now.AddDays(-1)
            };

            var result = validator.Validate(request);
            
            Assert.False(result.IsValid);
            Assert.True(result.Errors.Any());
            Assert.Equal("The journey departure time must be later than the current time", result.Errors.FirstOrDefault()?.ErrorMessage);
        }
        
        [Fact] 
        public void JourneySearchRequestValidator_Fails_WhenTakeLessThan1()
        {
            var validator = new JourneySearchRequestValidator();

            var request = new JourneySearchRequest()
            {
                Destination = new JourneySearchRequestLocation
                {
                    Code = "123"
                },
                Origin = new JourneySearchRequestLocation
                {
                    Code = "123"
                },
                DepartureTime = DateTimeOffset.Now.AddDays(1),
                Take = -598
            };

            var result = validator.Validate(request);
            
            Assert.False(result.IsValid);
            Assert.True(result.Errors.Any());
            Assert.Equal("There must be more than 0 journeys taken per request.", result.Errors.FirstOrDefault()?.ErrorMessage);
        }
        
        [Fact] 
        public void JourneySearchRequestValidator_Succeeds()
        {
            var validator = new JourneySearchRequestValidator();

            var request = new JourneySearchRequest()
            {
                Destination = new JourneySearchRequestLocation
                {
                    Code = "123"
                },
                Origin = new JourneySearchRequestLocation
                {
                    Code = "123"
                },
                DepartureTime = DateTimeOffset.Now.AddDays(1),
                Take = 598
            };

            var result = validator.Validate(request);
            
            Assert.True(result.IsValid);
            Assert.False(result.Errors.Any());
        }
    }
}