﻿using System.Linq;
using Search.Api.ApiModels;
using Search.Api.ApiModels.Validators;
using Xunit;

namespace SearchService.Test.ApiModels.Validators
{
    public class JourneySearchRequestLocationValidatorTests
    {
        [Theory]
        [InlineData(LocationType.Destination)]
        [InlineData(LocationType.Origin)]
        public void JourneySearchRequestLocationValidator_WhenLocationCodeIsEmpty(LocationType locationType)
        {
            var request = new JourneySearchRequestLocation();

            var validator = new JourneySearchRequestLocationValidator(locationType);
            
            var result = validator.Validate(request);
            
            Assert.False(result.IsValid);
            Assert.True(result.Errors.Any());
            Assert.Equal($"'{locationType.ToString()}' location code must not be null or empty.", result.Errors.FirstOrDefault()?.ErrorMessage);
        }
        
        [Fact]
        public void JourneySearchRequestLocationValidator_Succeeds()
        {
            var request = new JourneySearchRequestLocation()
            {
                Code = "123"
            };
            const LocationType locationType = LocationType.Destination;

            var validator = new JourneySearchRequestLocationValidator(locationType);
            
            var result = validator.Validate(request);
            
            Assert.True(result.IsValid);
            Assert.False(result.Errors.Any());
        }
    }
}