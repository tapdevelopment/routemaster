﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Search.Api.ApiModels;
using Search.Api.Contracts.Services;
using Search.Api.Controllers;
using Search.Api.Services;
using Xunit;

namespace SearchService.Test.Controllers
{
    public class JourneySearchControllerTests
    {
        private Mock<IJourneySearchService> _mockJourneySearchService;
        private Mock<IMapper<PerformJourneySearchResponse, JourneySearchResponse>> _mockServiceMapper;

        private JourneySearchController _controller;
        
        private void SetupMocks()
        {
            _mockJourneySearchService = new Mock<IJourneySearchService>();
            _mockServiceMapper = new Mock<IMapper<PerformJourneySearchResponse, JourneySearchResponse>>();

            _controller = new JourneySearchController(_mockJourneySearchService.Object, _mockServiceMapper.Object);
        }

        [Fact]
        public void SearchForJourney_ReturnsBadRequest_WhenValidationFails()
        {
            SetupMocks();
            
            var request = new JourneySearchRequest
            {
                Take = -500
            };

            var response = _controller.SearchForJourney(request) as BadRequestObjectResult;
            
            Assert.NotNull(response);
            Assert.True(response.StatusCode == (int) HttpStatusCode.BadRequest);
        }
        
        [Fact]
        public void SearchForJourney_ReturnsInternalError_WhenSearchFails()
        {
            SetupMocks();

            var origin = new JourneySearchRequestLocation
            {
                Code = "123"
            };
            var destination = new JourneySearchRequestLocation
            {
                Code = "233"
            };

            var departureDate = DateTimeOffset.Now.AddDays(1);
            
            var request = new JourneySearchRequest
            {
                DepartureTime = departureDate,
                Origin = origin,
                Destination = destination
            };

            _mockJourneySearchService
                .Setup(mjss =>
                    mjss.PerformSearch(
                        It.Is<PerformJourneySearchRequest>(pjsr => pjsr.DestinationCode == destination.Code 
                                                                   && pjsr.OriginCode == origin.Code && pjsr.DepartureTime == departureDate)))
                .Throws(new Exception("test"));
            
            var exception = Assert.Throws<Exception>(() => _controller.SearchForJourney(request));
            Assert.Equal("test", exception.Message);
        }
        
        [Fact]
        public void SearchForJourney_ReturnsOk()
        {
            SetupMocks();

            var origin = new JourneySearchRequestLocation
            {
                Code = "123"
            };
            var destination = new JourneySearchRequestLocation
            {
                Code = "233"
            };

            var departureDate = DateTimeOffset.Now.AddDays(1);
            var arrivalDate = departureDate.AddDays(1);
            
            var request = new JourneySearchRequest
            {
                DepartureTime = departureDate,
                Origin = origin,
                Destination = destination
            };

            var serviceResponse = new PerformJourneySearchResponse
            {
                Journeys = new List<PerformJourneySearchResult>
                {
                    new()
                    {
                        DepartureTime = departureDate,
                        ArrivalTime = arrivalDate,
                        DestinationCode = destination.Code,
                        OriginCode = origin.Code
                    }
                }
            };

            var response = new JourneySearchResponse
            {
                Journeys = new List<Journey>
                {
                    new()
                    {
                        DepartureTime = departureDate,
                        ArrivalTime = arrivalDate,
                        DestinationCode = destination.Code,
                        OriginCode = origin.Code
                    }
                }
            };

            _mockJourneySearchService
                .Setup(mjss =>
                    mjss.PerformSearch(
                        It.Is<PerformJourneySearchRequest>(pjsr => pjsr.DestinationCode == destination.Code
                                                                   && pjsr.OriginCode == origin.Code &&
                                                                   pjsr.DepartureTime == departureDate)))
                .Returns(serviceResponse);
            _mockServiceMapper
                .Setup(msm => msm.Convert(It.IsAny<PerformJourneySearchResponse>()))
                .Returns(response);

            var methodResponse = _controller.SearchForJourney(request) as ObjectResult;

            var methodResponseValue = methodResponse?.Value as JourneySearchResponse;
            var responseJourney = methodResponseValue?.Journeys.FirstOrDefault();
            
            Assert.NotNull(methodResponse);
            Assert.Equal((int) HttpStatusCode.OK, methodResponse.StatusCode);
            
            Assert.Equal(responseJourney?.DepartureTime, departureDate);
            Assert.Equal(responseJourney?.OriginCode, origin.Code);
            Assert.Equal(responseJourney?.DestinationCode, destination.Code);
        }
    }
}