﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure;
using Moq;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;
using Search.Api.Repositories;
using Search.Api.Services;
using SearchService.Test.Misc.Builders;
using Xunit;

namespace SearchService.Test.Services
{
    public class UkJourneySearchServiceTests
    {
        private Mock<IJourneySearchRepository> _mockJourneySearchRepository;
        private Mock<IMapper<JourneyResult, PerformJourneySearchResult>> _mockRepositoryMapper;

        private UkJourneySearchService _service;
        
        private void SetupMocks()
        {
            _mockJourneySearchRepository = new Mock<IJourneySearchRepository>();
            _mockRepositoryMapper = new Mock<IMapper<JourneyResult, PerformJourneySearchResult>>();

            _service = new UkJourneySearchService(_mockJourneySearchRepository.Object, _mockRepositoryMapper.Object);
        }

        [Fact]
        public void PerformSearch_RepositoryCallFails()
        {
            SetupMocks();
            
            var originCode = "123";
            var destinationCode = "234";
            var departureDate = DateTimeOffset.Now.AddDays(1);
            
            var request = new PerformJourneySearchRequest
            {
                OriginCode = originCode,
                DestinationCode = destinationCode,
                DepartureTime = departureDate
            };

            _mockJourneySearchRepository.Setup(mjsr =>
                mjsr.SearchForJourneys(departureDate, originCode, destinationCode, null))
                .Throws(new Exception("test"));

            var exception = Assert.Throws<Exception>(() => _service.PerformSearch(request));
            Assert.Equal("test", exception.Message);
        }
        
        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10)]
        public void PerformSearch_Succeeds(int valuesCount)
        {
            SetupMocks();
            
            var originCode = "123";
            var destinationCode = "234";
            var departureTime = DateTimeOffset.Now.AddDays(1);
            var arrivalTime = departureTime.AddHours(6);
            
            var request = new PerformJourneySearchRequest
            {
                OriginCode = originCode,
                DestinationCode = destinationCode,
                DepartureTime = departureTime
            };

            var response = new PerformJourneySearchResponse
            {
                Journeys = new List<PerformJourneySearchResult>()
            };

            var repositoryResponse = new List<JourneyResult>();

            for (var i = 0; i < valuesCount; i++)
            {
                var enrichedDestinationCode = destinationCode + i;
                
                var serviceValue =
                    new PerformJourneySearchResultBuilder()
                        .WithArrivalTime(arrivalTime)
                        .WithDepartureTime(departureTime)
                        .WithDestinationCode(enrichedDestinationCode)
                        .WithOriginCode(originCode + i)
                        .Build();
                
                var repositoryValue =
                    new JourneyResultBuilder()
                        .WithArrivalTime(arrivalTime)
                        .WithDepartureTime(departureTime)
                        .WithDestinationCode(enrichedDestinationCode)
                        .WithOriginCode(originCode + i)
                        .Build();

                _mockRepositoryMapper
                    .Setup(mrm => mrm.Convert(It.Is<JourneyResult>(jr => jr.Destination == enrichedDestinationCode)))
                    .Returns(serviceValue);

                response.Journeys.Add(serviceValue);
                repositoryResponse.Add(repositoryValue);
            }

            _mockJourneySearchRepository.Setup(mjsr =>
                    mjsr.SearchForJourneys(departureTime, originCode, destinationCode, null))
                .Returns(repositoryResponse);

            var methodResponse = _service.PerformSearch(request);
            var journeysResponse = methodResponse.Journeys;
            var journeysDestinationCodes = journeysResponse.Select(j => j.DestinationCode).ToHashSet();
            var journeysOriginCodes = journeysResponse.Select(j => j.OriginCode).ToHashSet();
            
            Assert.Equal(journeysResponse.Count, valuesCount);
            Assert.All(journeysResponse, jr => Assert.Equal(jr.ArrivalTime, arrivalTime));
            Assert.All(journeysResponse, jr => Assert.Equal(jr.DepartureTime, departureTime));
            Assert.Equal(journeysDestinationCodes.Distinct().Count(), journeysDestinationCodes.Count);
            Assert.Equal(journeysOriginCodes.Distinct().Count(), journeysOriginCodes.Count);
        }
    }
}