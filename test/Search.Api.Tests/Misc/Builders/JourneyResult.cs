﻿using System;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;

namespace SearchService.Test.Misc.Builders
{
    public class JourneyResultBuilder
    {
        private readonly JourneyResult _result = new JourneyResult();

        public JourneyResultBuilder WithOriginCode(string code)
        {
            _result.Origin = code;

            return this;
        }
        
        public JourneyResultBuilder WithDestinationCode(string code)
        {
            _result.Destination = code;

            return this;
        }
        
        public JourneyResultBuilder WithDepartureTime(DateTimeOffset departureTime)
        {
            _result.DepartureTime = departureTime;

            return this;
        }
        
        public JourneyResultBuilder WithArrivalTime(DateTimeOffset arrivalTime)
        {
            _result.ArrivalTime = arrivalTime;

            return this;
        }

        public JourneyResult Build()
        {
            return _result;
        }
    }
}