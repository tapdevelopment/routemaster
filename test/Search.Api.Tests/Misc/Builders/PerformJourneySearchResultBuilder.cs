﻿using System;
using Search.Api.Contracts.Services;

namespace SearchService.Test.Misc.Builders
{
    public class PerformJourneySearchResultBuilder
    {
        private readonly PerformJourneySearchResult _result = new PerformJourneySearchResult();

        public PerformJourneySearchResultBuilder WithOriginCode(string code)
        {
            _result.OriginCode = code;

            return this;
        }
        
        public PerformJourneySearchResultBuilder WithDestinationCode(string code)
        {
            _result.DestinationCode = code;

            return this;
        }
        
        public PerformJourneySearchResultBuilder WithDepartureTime(DateTimeOffset departureTime)
        {
            _result.DepartureTime = departureTime;

            return this;
        }
        
        public PerformJourneySearchResultBuilder WithArrivalTime(DateTimeOffset arrivalTime)
        {
            _result.ArrivalTime = arrivalTime;

            return this;
        }

        public PerformJourneySearchResult Build()
        {
            return _result;
        }
    }
}