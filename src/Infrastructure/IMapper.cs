﻿namespace Infrastructure
{
    public interface IMapper<in TSource, out TDestination>
    {
        TDestination Convert(TSource sourceObject);
    }
}