﻿using System;

namespace Search.Api.Contracts.Services
{
    public class PerformJourneySearchRequest
    {
        public string OriginCode { get; set; }
        public string DestinationCode { get; set; }
        public DateTimeOffset DepartureTime { get; set; }
        public int? Take { get; set; }
    }
}