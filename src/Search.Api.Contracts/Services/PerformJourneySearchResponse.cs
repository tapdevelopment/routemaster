﻿using System.Collections.Generic;

namespace Search.Api.Contracts.Services
{
    public class PerformJourneySearchResponse
    {
        public ICollection<PerformJourneySearchResult> Journeys { get; set; }
    }
}