﻿using System;

namespace Search.Api.Contracts.Services
{
    public class PerformJourneySearchResult
    {
        public string OriginCode { get; set; }
        public string DestinationCode { get; set; }
        public DateTimeOffset DepartureTime { get; set; }
        public DateTimeOffset ArrivalTime { get; set; }
    }
}