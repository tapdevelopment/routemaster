﻿using Infrastructure;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;

namespace Search.Api.Contracts.Mappers.Repositories
{
    public class JourneyResultMapper : IMapper<JourneyResult, PerformJourneySearchResult>
    {
        public PerformJourneySearchResult Convert(JourneyResult sourceObject)
        {
            return new PerformJourneySearchResult
            {
                OriginCode = sourceObject.Origin,
                ArrivalTime = sourceObject.ArrivalTime,
                DepartureTime = sourceObject.DepartureTime,
                DestinationCode = sourceObject.Destination
            };
        }
    }
}