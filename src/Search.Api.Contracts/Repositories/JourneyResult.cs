﻿using System;

namespace Search.Api.Contracts.Repositories
{
    public class JourneyResult
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTimeOffset DepartureTime { get; set; }
        public DateTimeOffset ArrivalTime { get; set; }
    }
}