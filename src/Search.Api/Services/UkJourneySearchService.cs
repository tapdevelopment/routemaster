﻿using System;
using System.Linq;
using Infrastructure;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;
using Search.Api.Repositories;

namespace Search.Api.Services
{
    public class UkJourneySearchService : IJourneySearchService
    {
        private readonly IMapper<JourneyResult, PerformJourneySearchResult> _journeyRepositoryMapper;
        
        private readonly IJourneySearchRepository _journeySearchRepository;
        
        public UkJourneySearchService(IJourneySearchRepository journeySearchRepository, IMapper<JourneyResult, PerformJourneySearchResult> journeyRepositoryMapper)
        {
            _journeySearchRepository = journeySearchRepository;
            _journeyRepositoryMapper = journeyRepositoryMapper;
        }
        
        public PerformJourneySearchResponse PerformSearch(PerformJourneySearchRequest request)
        {
            // we don't want to always return ALL journeys in our index, it's more useful
            // to return them in "pages" from the client's perspective
            var journeys = _journeySearchRepository
                .SearchForJourneys(request.DepartureTime, request.OriginCode, request.DestinationCode, request.Take);
            
            return new PerformJourneySearchResponse
            {
                Journeys = journeys
                    .Select(j => _journeyRepositoryMapper.Convert(j))
                    .ToList()
            };
        }
    }
}