﻿using Search.Api.Contracts.Services;

namespace Search.Api.Services
{
    public interface IJourneySearchService
    {
        PerformJourneySearchResponse PerformSearch(PerformJourneySearchRequest request);
    }
}