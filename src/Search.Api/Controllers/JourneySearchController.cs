﻿using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Search.Api.ApiModels;
using Search.Api.ApiModels.Validators;
using Search.Api.Contracts.Services;
using Search.Api.Services;
using JourneySearchRequest = Search.Api.ApiModels.JourneySearchRequest;

namespace Search.Api.Controllers
{
    [ApiController]
    [Route("search/journey")]
    public class JourneySearchController : ControllerBase
    {
        private readonly IJourneySearchService _journeySearchService;
        private readonly IMapper<PerformJourneySearchResponse, JourneySearchResponse> _responseMapper;

        public JourneySearchController(IJourneySearchService journeySearchService, IMapper<PerformJourneySearchResponse, JourneySearchResponse> responseMapper)
        {
            _journeySearchService = journeySearchService;
            _responseMapper = responseMapper;
        }
        
        [Route("")]
        [HttpPost]
        public IActionResult SearchForJourney([FromBody] JourneySearchRequest request)
        {
            var journeySearchRequestValidator = new JourneySearchRequestValidator();

            var validationResult = journeySearchRequestValidator.Validate(request);

            if (!validationResult.IsValid)
            {
                // in real world code we would've returned a simpler object. User doesn't need to know about 
                // all the request validation details
                return BadRequest(validationResult.Errors);
            }
            
            var journeyCodes = request.ExtractJourneyCodes();
            
            var journeyResults = _journeySearchService.PerformSearch(new PerformJourneySearchRequest
            {
                OriginCode = journeyCodes.OriginCode,
                DestinationCode = journeyCodes.DestinationCode,
                DepartureTime = request.DepartureTime,
                Take = request.Take
            });
            
            var response = _responseMapper.Convert(journeyResults);
            return Ok(response);
        }
    }
}