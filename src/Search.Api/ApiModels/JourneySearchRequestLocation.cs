﻿using System.Collections.Generic;

namespace Search.Api.ApiModels
{
    public class JourneySearchRequestLocation
    {
        public string Code { get; set; }
        public IEnumerable<string> Regions { get; set; }
    }
}