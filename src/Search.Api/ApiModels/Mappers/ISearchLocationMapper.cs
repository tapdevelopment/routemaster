﻿using Infrastructure;

namespace Search.Api.ApiModels.Mappers
{
    public class UkSearchLocationMapper : IMapper<JourneySearchRequestLocation, string>
    {
        public string Convert(JourneySearchRequestLocation sourceObject)
        {
            return sourceObject.Code;
        }
    }
}