﻿using System.Linq;
using Infrastructure;
using Search.Api.Contracts.Services;

namespace Search.Api.ApiModels.Mappers
{
    public class JourneySearchResponseMapper : IMapper<PerformJourneySearchResponse, JourneySearchResponse>
    {
        public JourneySearchResponse Convert(PerformJourneySearchResponse location)
        {
            return new()
            {
                Journeys = location.Journeys.Select(inputJourney => new Journey
                {
                    OriginCode = inputJourney.OriginCode,
                    DestinationCode = inputJourney.DestinationCode,
                    DepartureTime = inputJourney.DepartureTime,
                    ArrivalTime = inputJourney.ArrivalTime,
                }).ToList(),
            };
        }
    }
}