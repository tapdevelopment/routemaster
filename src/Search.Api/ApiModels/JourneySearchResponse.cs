﻿using System;
using System.Collections.Generic;

namespace Search.Api.ApiModels
{
    public class JourneySearchResponse
    {
        public IEnumerable<Journey> Journeys { get; set; }
    }

    public class Journey
    {
        public string OriginCode { get; set; }
        public string DestinationCode { get; set; }
        public DateTimeOffset DepartureTime { get; set; }
        public DateTimeOffset ArrivalTime { get; set; }
    }
}