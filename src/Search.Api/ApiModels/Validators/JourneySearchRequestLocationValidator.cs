﻿using FluentValidation;

namespace Search.Api.ApiModels.Validators
{
    public class JourneySearchRequestLocationValidator : AbstractValidator<JourneySearchRequestLocation>
    {
        public JourneySearchRequestLocationValidator(LocationType locationType)
        {
            RuleFor(x => x.Code)
                .Must(code => !string.IsNullOrEmpty(code))
                .WithMessage($"'{locationType.ToString()}' location code must not be null or empty.");
        }
    }
}