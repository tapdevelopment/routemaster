﻿using System;
using FluentValidation;

namespace Search.Api.ApiModels.Validators
{
    public class JourneySearchRequestValidator : AbstractValidator<JourneySearchRequest>
    {
        public JourneySearchRequestValidator()
        {
            RuleFor(x => x.Origin)
                .NotNull()
                .SetValidator(new JourneySearchRequestLocationValidator(LocationType.Origin));
            
            RuleFor(x => x.Destination)
                .NotNull()
                .SetValidator(new JourneySearchRequestLocationValidator(LocationType.Destination));
            
            // I would've added a 1+ day book though. Nobody books journeys 1 hours before departure
            RuleFor(x => x.DepartureTime)
                .Must(departureTime => departureTime > DateTimeOffset.Now)
                .WithMessage("The journey departure time must be later than the current time");
            
            RuleFor(x => x.Take)
                .GreaterThan(0)
                .WithMessage("There must be more than 0 journeys taken per request.");
        }
    }
}