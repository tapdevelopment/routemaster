﻿namespace Search.Api.ApiModels.Validators
{
    public enum LocationType
    {
        Origin,
        Destination
    }
}