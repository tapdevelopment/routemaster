﻿namespace Search.Api.ApiModels
{
    public class JourneySearchRequestLocationCodes
    {
        public string OriginCode { get; set; }
        
        public string DestinationCode { get; set; }
    }
}