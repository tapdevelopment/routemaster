﻿using System;

namespace Search.Api.ApiModels
{
    public class JourneySearchRequest
    {
        public JourneySearchRequestLocation Origin { get; set; }
        public JourneySearchRequestLocation Destination { get; set; }
        public DateTimeOffset DepartureTime { get; set; }
        public int? Take { get; set; }

        public JourneySearchRequestLocationCodes ExtractJourneyCodes()
        {
            return new()
            {
                OriginCode = Origin.Code,
                DestinationCode = Destination.Code
            };
        }
    }
}