﻿using Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Search.Api.ApiModels;
using Search.Api.ApiModels.Mappers;
using Search.Api.Contracts.Mappers.Repositories;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;
using Search.Api.Repositories;
using Search.Api.Services;

namespace Search.Api
{
    public static class RegistrationExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IJourneySearchService, UkJourneySearchService>();
        }

        public static void AddMappers(this IServiceCollection services)
        {
            services.AddTransient<IMapper<JourneySearchRequestLocation, string>, UkSearchLocationMapper>();
            services.AddTransient<IMapper<PerformJourneySearchResponse, JourneySearchResponse>, JourneySearchResponseMapper>();
            services.AddTransient<IMapper<JourneyResult, PerformJourneySearchResult>, JourneyResultMapper>();
        }
        
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IJourneySearchRepository, RandomJourneySearchRepository>();
        }
    }
}