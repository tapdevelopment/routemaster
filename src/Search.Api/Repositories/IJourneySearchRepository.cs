﻿using System;
using System.Collections.Generic;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;

namespace Search.Api.Repositories
{
    public interface IJourneySearchRepository
    {
        IEnumerable<JourneyResult> SearchForJourneys(DateTimeOffset departureTime, string origin,
            string destination, int? take = null);
    }
}