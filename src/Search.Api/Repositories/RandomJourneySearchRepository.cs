﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using Search.Api.Contracts.Repositories;
using Search.Api.Contracts.Services;

namespace Search.Api.Repositories
{
    public class RandomJourneySearchRepository : IJourneySearchRepository
    {
        private readonly AppSettings _settings;
        
        private readonly Random _randomGenerator = new ();
        
        private const int JourneyCount = 6;
        private const int RandomisationLimit = 4;

        public RandomJourneySearchRepository(IOptions<AppSettings> settings)
        {
            _settings = settings.Value;
        }

        public IEnumerable<JourneyResult> SearchForJourneys(DateTimeOffset departureTime, string origin, string destination,
            int? take = null)
        {
            take ??= _settings.DefaultJourneysTake;
            
            var departureTimeAddition = _randomGenerator.Next(RandomisationLimit);
            var arrivalTimeAddition = departureTimeAddition + _randomGenerator.Next(1, RandomisationLimit);

            var journeys = new HashSet<JourneyResult>();

            for (var i = 0; i < JourneyCount; i++)
            {
                var innerDepartureTime = departureTime.AddDays(departureTimeAddition);
                var arrivalTime = innerDepartureTime.AddHours(arrivalTimeAddition);
                
                journeys.Add(new JourneyResult
                {
                    Origin = origin,
                    Destination = destination,
                    DepartureTime = innerDepartureTime,
                    ArrivalTime = arrivalTime,
                });
            }

            return journeys.Take(take.Value);
        }
    }
}