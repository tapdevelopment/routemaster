using System;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.OpenApi.Models;

namespace Search.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Search.Api", Version = "v1"});
            });
            
            services.AddLogging(builder =>
            {
                builder
                    .AddConfiguration(Configuration)
                    .AddSimpleConsole(options =>
                    {
                        options.ColorBehavior = LoggerColorBehavior.Disabled;
                    });
            });
            
            services.Configure<AppSettings>(options =>
            {
                options.DefaultJourneysTake = Convert.ToInt32(Configuration["Default_MaximalJourneys"]);
            });
            
            services.AddServices();
            services.AddRepositories();
            
            services.AddMappers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Search.Api v1"));
            }
            
            app.UseExceptionHandler(
                options =>
                {
                    options.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";
                        var exceptionObject = context.Features.Get<IExceptionHandlerFeature>();
                        if (exceptionObject != null)
                        {
                            var errorMessage = $"{exceptionObject.Error.Message}";
                            var logger = options.ApplicationServices.GetService<ILogger<Startup>>();
                            logger.LogError(errorMessage);
                            
                            await context.Response.WriteAsync("An internal error has occured.").ConfigureAwait(false);
                        }});
                }
            );

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}